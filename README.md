# Application Android Coach 

** _Note sur l'avancement du projet_ **

L'application est terminée.

## ** Technologies utilisées **

WampServer pour le test de la connexion à la base de données SQL

Android Studio avec Android 9.0 (Pie) 

Java : OpenJDK version "1.8.0_152-release"

## ** But de l'application **

Cette application Android exploitant le pattern MVC propose à l'utilisateur de calculer son Indice de Masse Corporelle en rentrant son âge, son genre, sa taille et son poids. 
Les mesures entrées sont mémorisées dans une base de données distante au format MySQL.
L'historique des mesures est également disponible depuis le menu, avec possibilité de les retirer de la BDD en cliquant sur la croix rouge.

## ** Utiliser et tester l'application **

L'application n'est pas disponible en téléchargement sur le Play store. Vous pouvez cependant récupérer les sources via BitBucket.

Veuillez noter que pour tester la connexion à la BDD distante vous aurez à modifier certains paramètres.

### 1 / Fichiers PHP 

Récupérer les fichiers dans le dossier PHP du projet  : les copier dans un nouveau dossier à la racine du serveur (www pour wamp par exemple).
Modifier si besoin les paramètres de connexion dans le fichier fonctions.php. Actuellement les paramètres sont ceux d'un test sur Wamp

### 2 / accesDistant.java

Package Modele, accesDistant : modifier la propriété statique suivante pour indiquer l'adresse du fichier php serveurcoach, normalement copié à la racine de votre serveur 
private static final String SERVERADDR = "http://XXX.XXX.X.XX/coach/serveurcoach.php";