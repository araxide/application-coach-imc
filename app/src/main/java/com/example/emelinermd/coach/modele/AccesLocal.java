package com.example.emelinermd.coach.modele;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.emelinermd.coach.outils.MesOutils;
import com.example.emelinermd.coach.outils.MySQLiteOpenHelper;

import java.util.Date;

public class AccesLocal {

    // propriétés
    private String nomBase = "bdCoach.sqlite";
    private Integer versionBase = 1;
    MySQLiteOpenHelper accesBD;
    private SQLiteDatabase bd;

    /**
     * Constructeur : crée l'accès à la BD
     * @param context
     */
    public AccesLocal(Context context){
        this.accesBD = new MySQLiteOpenHelper(context, nomBase, versionBase);
    }

    /**
     * Ajout d'un profil dans la BD
     * @param profil
     */
    public void ajout(Profil profil){
        this.bd = accesBD.getWritableDatabase();
        String req = "insert into profil (dateMEsure, poids, taille, age, sexe) values ";
        req += "(\"" + profil.getDateMesure() + "\"," + profil.getPoids() + "," + profil.getTaille() + "," + profil.getAge() + "," + profil.getSexe() + ")";
        bd.execSQL(req);
    }


    /**
     * Récupère le dernier profil de la BD
     * @return
     */
    public Profil recupDernier(){
        Profil profil = null;
        this.bd = accesBD.getReadableDatabase();
        String req = "select * from profil";
        Cursor curseur = bd.rawQuery(req, null);
        // positionnement sur la dernière ligne du curseur
        curseur.moveToLast();
        // contrôle s'il y a bien au moins une ligne
        if(!curseur.isAfterLast()){
            // récupération des informations du dernier profil
            Date dateMesure = MesOutils.convertStringToDate(curseur.getString(0));
            Log.d("date", "*************************conversion : "+dateMesure);
            Integer poids = curseur.getInt(1);
            Integer taille = curseur.getInt(2);
            Integer age = curseur.getInt(3);
            Integer sexe = curseur.getInt(4);
            // création de l'objet profil
            profil = new Profil(dateMesure, poids, taille, age, sexe);
        }
        // fermeture du curseur
        curseur.close();
        // retourne le profil
        return profil;
    }
}
