package com.example.emelinermd.coach.modele;

import android.support.annotation.NonNull;

import com.example.emelinermd.coach.outils.MesOutils;

import org.json.JSONArray;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Profil implements Serializable, Comparable {

    // constantes
    private static final Integer minFemme = 15; // maigre si en dessous
    private static final Integer maxFemme = 30; // gros si au dessus
    private static final Integer minHomme = 10; // maigre si en dessous
    private static final Integer maxHomme = 25; // gros si au dessus

    // propriétés
    private Integer poids;
    private Integer taille;
    private Integer age;
    private Integer sexe;
    private float img;
    private String message;
    private Date dateMesure;

    /**
     * Constructeur : valorise poids, taille, age, sexe
     * @param poids
     * @param taille
     * @param age
     * @param sexe
     */
    public Profil(Date dateMesure, Integer poids, Integer taille, Integer age, Integer sexe) {
        this.dateMesure = dateMesure;
        this.poids = poids;
        this.taille = taille;
        this.age = age;
        this.sexe = sexe;
        // valorisation de l'img et du message
        calculIMG();
        resultIMG();
    }

    /**
     * getter
     * @return poids
     */
    public Integer getPoids() {
        return poids;
    }

    /**
     * getter
     * @return taille
     */
    public Integer getTaille() {
        return taille;
    }

    /**
     * getter
     * @return age
     */
    public Integer getAge() {
        return age;
    }

    /**
     * getter
     * @return sexe
     */
    public Integer getSexe() {
        return sexe;
    }

    /**
     * getter
     * @return img
     */
    public float getImg() {
        return img;
    }

    /**
     * getter
     * @return message
     */
    public String getMessage() {
        return message;
    }

    /**
     * getter
     * @return dateMesure
     */
    public Date getDateMesure() {
        return dateMesure;
    }

    /**
     * Calcul de l'IMG
     */
    private void calculIMG(){
        float tailleEnCm = ((float)taille)/100;
        img = (float)((1.2*poids/(tailleEnCm*tailleEnCm))+(0.23*age)-(10.83*sexe)-5.4);
    }

    /**
     * Valorisation du résultat en fonction de l'img, du sexe et des bornes (constantes)
     */
    private void resultIMG(){
        message = "normal";
        Integer min = minFemme, max = maxFemme;
        if(sexe == 1){
            min = minHomme;
            max = maxHomme;
        }
        if(img<min){
            message = "trop faible";
        }else{
            if(img>max){
                message = "trop élevé";
            }
        }
    }

    /**
     * Conversion du profil au format JSONArray
     * @return
     */
    public JSONArray convertToJSONArray(){
        List liste = new ArrayList();
        liste.add(MesOutils.convertDateToString(dateMesure));
        liste.add(poids);
        liste.add(taille);
        liste.add(age);
        liste.add(sexe);
        return new JSONArray(liste);
    }

    /**
     * Comparaidon de 2 profils sur les dates
     * @param o
     * @return
     */
    @Override
    public int compareTo(@NonNull Object o) {
        return dateMesure.compareTo(((Profil)o).getDateMesure());
    }
}
