package com.example.emelinermd.coach.outils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by emelinermd on 14/04/2019.
 */

public abstract class MesOutils {

    /**
     * Converti une date de String en Date (format "EEE MMM dd hh:mm:ss 'GMT+00:00' yyyy")
     * @param uneDate date à convertir
     * @return date convertie
     */
    public static Date convertStringToDate(String uneDate){
        String expectedPattern = "EEE MMM dd hh:mm:ss 'GMT+00:00' yyyy";
        return convertStringToDate(uneDate, "EEE MMM dd hh:mm:ss 'GMT+00:00' yyyy");
    }

    /**
     * Converti une date de String en Date (au format reçu en paramètre)
     * @param uneDate à convertir
     * @param expectedPattern format de conversion
     * @return date convertie
     */
    public static Date convertStringToDate(String uneDate, String expectedPattern){
        SimpleDateFormat formatter = new SimpleDateFormat(expectedPattern);
        try {
            Date date = formatter.parse(uneDate);
            return date;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Converti une date du format Date en String (format "yyyy-MM-dd HH:mm:ss")
     * @param uneDate
     * @return
     */
    public static String convertDateToString(Date uneDate){
        SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return date.format(uneDate);
    }

    /**
     * Formate 2 chiffres après la virgule et retourne la valeur en string
     * @param valeur float à formater
     * @return valeur formatée et en String
     */
    public static String format2Decimal(Float valeur){
        return String.format("%.01f", valeur);
    }

}
