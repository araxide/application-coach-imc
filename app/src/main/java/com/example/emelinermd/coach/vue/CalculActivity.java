package com.example.emelinermd.coach.vue;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.emelinermd.coach.R;
import com.example.emelinermd.coach.controleur.Controle;
import com.example.emelinermd.coach.outils.MesOutils;

public class CalculActivity extends AppCompatActivity {

    // propriétés
    private EditText txtTaille;
    private EditText txtPoids;
    private EditText txtAge;
    private RadioButton rdHomme;
    private RadioButton rdFemme;
    private TextView lblIMG;
    private ImageView imgSmiley;
    private Controle controle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calcul);
        init();
    }

    /**
     * Lien entre les propriétés et les objets graphiques
     * et lien avec le controleur
     */
    private void init(){
        txtPoids = (EditText) findViewById(R.id.txtPoids) ;
        txtTaille = (EditText) findViewById(R.id.txtTaille) ;
        txtAge = (EditText) findViewById(R.id.txtAge) ;
        rdHomme = (RadioButton) findViewById(R.id.rdHomme) ;
        rdFemme = (RadioButton) findViewById(R.id.rdFemme) ;
        lblIMG = (TextView) findViewById(R.id.lblIMG) ;
        imgSmiley = (ImageView) findViewById(R.id.imgSmiley) ;
        // lien avec le contrôleur
        controle = Controle.getInstance(this);
        // attente de l'événement sur le clic du bouton calcul
        ecouteCalcul();
        ecouteRetourAccueil();
        // tentative de récupératon du profil précédemment sérialisé
        recupProfil();;
    }

    /**
     * Ecoute de l'événement sur le bouton calcul
     */
    private void ecouteCalcul(){
        ((Button) findViewById(R.id.btnCalc)).setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                // Toast.makeText(CalculActivity.this, "test", Toast.LENGTH_SHORT).show();
                // récupération des informations saisies
                Integer taille = 0, poids = 0, age = 0;
                try {
                    poids = Integer.parseInt(txtPoids.getText().toString());
                    taille = Integer.parseInt(txtTaille.getText().toString());
                    age = Integer.parseInt(txtAge.getText().toString());
                }catch(Exception e){}
                // récupération du sexe sélectionné
                Integer sexe = 0; // femme
                if(rdHomme.isChecked()){
                    sexe = 1; // homme
                }
                // contrôle si tous les champs sont remplis
                if(poids==0 || taille==0 || age==0){
                    Toast.makeText(CalculActivity.this, "Veuillez saisir tous les champs", Toast.LENGTH_SHORT).show();
                }else{
                    afficheResult(poids, taille, age, sexe);
                }
            }
        });
    }

    /**
     * Afichage de la valeur de l'img, du message et de l'image correspondante
     * @param poids
     * @param taille
     * @param age
     * @param sexe
     */
    private void afficheResult(Integer poids, Integer taille, Integer age, Integer sexe){
        // demande au controleur de créer le profil avec les informations saisies
        controle.creerProfil(poids, taille, age, sexe, this);
        // récupération du calcul de l'img et du message
        float img = controle.getImg();
        String message = controle.getMessage();
        // affichage de la bonne image en fonction du message reçu
        if(message.equals("normal")) {
            imgSmiley.setImageResource(R.drawable.normal);
            lblIMG.setTextColor(Color.GREEN);
        }else {
            lblIMG.setTextColor(Color.RED);
            if (message.equals("trop faible")) {
                imgSmiley.setImageResource(R.drawable.maigre);
            } else {
                imgSmiley.setImageResource(R.drawable.graisse);
            }
        }
        // Affichage de la valeur de l'img et du message correspondant
        lblIMG.setText(MesOutils.format2Decimal(img)+" : IMG "+message);
    }

    /**
     * Récupère le profil sérialisé et affiche les informations
     */
    public void recupProfil(){
        if(controle.getTaille()!=null){
            // affichage des informations récupérées
            txtPoids.setText(controle.getPoids().toString());
            txtTaille.setText(controle.getTaille().toString());
            txtAge.setText(controle.getAge().toString());
            if(controle.getSexe()==0){
                rdFemme.setChecked(true);
            }else{
                rdHomme.setChecked(true);
            }
            // simulation du clic sur le bouton calcul
//            ((Button)findViewById(R.id.btnCalc)).performClick();
            // vider le profil
            controle.setProfil(null);
        }
    }

    /**
     * Evénement clic sur le bouton de retour vers l'accuel
     */
    private void ecouteRetourAccueil(){
        ((ImageButton)findViewById(R.id.btnRetourDeCalcul)).setOnClickListener(new ImageButton.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(CalculActivity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
    }
}
