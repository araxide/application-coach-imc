package com.example.emelinermd.coach.vue;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import com.example.emelinermd.coach.R;
import com.example.emelinermd.coach.controleur.Controle;

public class MainActivity extends AppCompatActivity {

    // propriété
    private Controle controle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.controle = Controle.getInstance(this);
        creerMenu();
    }

    /**
     * Création des événements sur les 2 images du menu
     */
    private void creerMenu(){
        ecouteMenu((ImageButton)findViewById(R.id.btnMonIMG), CalculActivity.class);
        ecouteMenu((ImageButton)findViewById(R.id.btnMonHistorique), HistoActivity.class);
    }

    /**
     * Evénement clic sur une image du menu
     * @param imageButton
     * @param classe
     */
    private void ecouteMenu(ImageButton imageButton, final Class classe){
        imageButton.setOnClickListener(new ImageButton.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, classe);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
    }
}
