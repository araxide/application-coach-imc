package com.example.emelinermd.coach.outils;

/**
 * Created by emelinermd on 14/04/2019.
 */
public interface AsyncResponse {
    void processFinish(String output); // cette classe force la redéfinition de processFinish
}
